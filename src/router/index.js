import Vue from 'vue';
import Router from 'vue-router';

import Home from '@/components/Home';
import Services from '@/components/Services';
import Prices from '@/components/Prices';
import Contact from '@/components/Contact';

Vue.use(Router)

export default new Router({
  routes: [
    { path:'/', name:'Home', component:Home },
    { path:'/services', name:'Services', component:Services },
    { path:'/prices', name:'Prices', component:Prices },
    { path:'/contact', name:'Contact', component:Contact }
  ],
  mode:'history'
});
