// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueResource from 'vue-resource';
import { library } from '@fortawesome/fontawesome-svg-core';
import { 
  faAngleUp, faCopyright, faPhone, faCompass, faEnvelope, faPlusSquare, faArrowRight, faSquare, faCircleNotch, faCheckCircle, 
  faArrowLeft, faCheck, faSyncAlt, faClock, faCircle, faSpinner, faArrowCircleUp 
} from '@fortawesome/free-solid-svg-icons';
import { faFacebook } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import * as VueGoogleMaps from 'vue2-google-maps'
import Snotify from 'vue-snotify';
import 'vue-snotify/styles/material.css';

Vue.use(VueResource);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAkSXq6mm_noGE3Z1Kv61vYIE7JuyVr0OI',
    libraries: 'places'
  }
});
Vue.use(Snotify, {
  toast: {
    timeout:10000,
    showProgressBar:false,
    bodyMaxLength:200
  }
});

Vue.config.productionTip = false

library.add(faAngleUp, faCopyright, faPhone, faCompass, faEnvelope, faPlusSquare, faArrowRight, faArrowLeft, faCircleNotch, 
  faCheckCircle, faCheck, faSyncAlt, faClock, faCircle, faFacebook, faSpinner, faArrowCircleUp);

Vue.component('font-awesome-icon', FontAwesomeIcon);

export const eventBus = new Vue();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
