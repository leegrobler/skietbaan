# L & W Shooting Range

> Originally created as a simple price calculator for a local shooting range and built entirely on Jquery and Twitter Bootstrap (see commit 0121012). It later became the first project I set out to complete in Vue.js. It was then that I decided to upgrade the price calculator and transform the website into a complete marketing tool for the business.

[Check it out](https://skietbaan-6feab.firebaseapp.com/)

## Run Locally

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

#### Author

Lee Grobler  
hello@lee-grobler.com  

### Technologies Used

 - HTML
 - CSS
 - Javascript
 - JQuery
 - Vue
 - FontAwesome
 - Google Maps
